import {AppRegistry, View, Text} from 'react-native';
import {name as appName} from './app.json';
import App from './src/ReactNativeMarquee';

AppRegistry.registerComponent(appName, () => App);